package main

import (
	"fmt"
	"sync"
)

func workers(max int) {
	var wg sync.WaitGroup
	var mu sync.Mutex
	var count int
	work := func(i int, c *int, mu *sync.Mutex) {
		mu.Lock()
		defer mu.Unlock()
		*c++
		fmt.Printf("Поток № %d выполнен.\n", i)
	}

	for i := 1; i <= max; i++ {
		wg.Add(1)
		go func(n int) {
			defer wg.Done()
			work(n, &count, &mu)
		}(i)
	}

	wg.Wait()
	fmt.Println("Counts:", count)
}

func main() {
	workers(1000)
}
