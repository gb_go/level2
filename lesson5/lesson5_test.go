package main

import (
	"math/rand"
	"sync"
	"testing"
)

/*
Протестируйте производительность операций чтения и записи на множестве
действительных чисел, безопасность которого обеспечивается sync.Mutex и
sync.RWMutex для разных вариантов использования: 10% запись, 90% чтение; 50%
запись, 50% чтение; 90% запись, 10% чтение
*/

const (
	testCount = 1000
)

type SetMutex struct {
	set map[float64]struct{}
	mu  *sync.Mutex
}

type SetRWMutex struct {
	set map[float64]struct{}
	mu  *sync.RWMutex
}

func NewSet() *SetMutex {
	return &SetMutex{
		set: map[float64]struct{}{},
		mu:  &sync.Mutex{},
	}
}

func NewRWSet() *SetRWMutex {
	return &SetRWMutex{
		set: map[float64]struct{}{},
		mu:  &sync.RWMutex{},
	}
}

func (s *SetMutex) Get(x float64) {
	s.mu.Lock()
	_ = s.set[x]
	s.mu.Unlock()
}

func (s *SetMutex) Set(x float64) {
	s.mu.Lock()
	s.set[x] = struct{}{}
	s.mu.Unlock()
}

func (s *SetRWMutex) Get(x float64) {
	s.mu.RLock()
	_ = s.set[x]
	s.mu.RUnlock()
}

func (s *SetRWMutex) Set(x float64) {
	s.mu.Lock()
	s.set[x] = struct{}{}
	s.mu.Unlock()
}

func BenchmarkMutexW50R50(b *testing.B) {
	var (
		wgR = &sync.WaitGroup{}
		wgW = &sync.WaitGroup{}
	)
	structSet := NewSet()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for k := 0; k < testCount; k++ {
			wgW.Add(1)
			go func() {
				structSet.Set(rand.Float64())
				wgW.Done()
			}()
			wgR.Add(1)
			go func() {
				structSet.Get(rand.Float64())
				wgR.Done()
			}()
		}
	}
	wgR.Wait()
	wgW.Wait()
}

func BenchmarkRWMutexW50R50(b *testing.B) {
	var (
		wgR = &sync.WaitGroup{}
		wgW = &sync.WaitGroup{}
	)
	structSet := NewRWSet()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for k := 0; k < testCount; k++ {
			wgW.Add(1)
			go func() {
				structSet.Set(rand.Float64())
				wgW.Done()
			}()
			wgR.Add(1)
			go func() {
				structSet.Get(rand.Float64())
				wgR.Done()
			}()
		}
	}
	wgR.Wait()
	wgW.Wait()
}

func BenchmarkMutexW90R10(b *testing.B) {
	var (
		wgR = &sync.WaitGroup{}
		wgW = &sync.WaitGroup{}
	)
	structSet := NewSet()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for k := 0; k < testCount; k++ {
			if k%10 != 0 {
				wgW.Add(1)
				go func() {
					structSet.Set(rand.Float64())
					wgW.Done()
				}()
			}
			if k%10 == 0 {
				wgR.Add(1)
				go func() {
					structSet.Get(rand.Float64())
					wgR.Done()
				}()
			}
		}
	}
	wgR.Wait()
	wgW.Wait()
}

func BenchmarkRWMutexW90R10(b *testing.B) {
	var (
		wgR = &sync.WaitGroup{}
		wgW = &sync.WaitGroup{}
	)
	structSet := NewRWSet()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for k := 0; k < testCount; k++ {
			if k%10 != 0 {
				wgW.Add(1)
				go func() {
					structSet.Set(rand.Float64())
					wgW.Done()
				}()
			}
			if k%10 == 0 {
				wgR.Add(1)
				go func() {
					structSet.Get(rand.Float64())
					wgR.Done()
				}()
			}
		}
	}
	wgR.Wait()
	wgW.Wait()
}

func BenchmarkMutexW10R90(b *testing.B) {
	var (
		wgR = &sync.WaitGroup{}
		wgW = &sync.WaitGroup{}
	)
	structSet := NewSet()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for k := 0; k < testCount; k++ {
			if k%10 == 0 {
				wgW.Add(1)
				go func() {
					structSet.Set(rand.Float64())
					wgW.Done()
				}()
			}
			if k%10 != 0 {
				wgR.Add(1)
				go func() {
					structSet.Get(rand.Float64())
					wgR.Done()
				}()
			}
		}
	}
	wgR.Wait()
	wgW.Wait()
}

func BenchmarkRWMutexW10R90(b *testing.B) {
	var (
		wgR = &sync.WaitGroup{}
		wgW = &sync.WaitGroup{}
	)
	structSet := NewRWSet()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for k := 0; k < testCount; k++ {
			if k%10 == 0 {
				wgW.Add(1)
				go func() {
					structSet.Set(rand.Float64())
					wgW.Done()
				}()
			}
			if k%10 != 0 {
				wgR.Add(1)
				go func() {
					structSet.Get(rand.Float64())
					wgR.Done()
				}()
			}
		}
	}
	wgR.Wait()
	wgW.Wait()
}
