// Package pan создан для вызова не явной паники.
//
// Pan ничего не возвращает
//
// Pan()
//
package pan

import "fmt"

// Pan фунция вызывает panic из-за деления на ноль.
func Pan() {
	var panSlice [5]int
	fmt.Println(1 / panSlice[4])
}
