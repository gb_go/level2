package main

import (
	"fmt"
	"os"
	"pan/pan"
	"time"
)

type ErrorStartTime struct {
	text     string
	err_time string
}

func (e *ErrorStartTime) Error() string {
	return fmt.Sprintf("error: %s\nat time: %s\n", e.text, e.err_time)
}

func New(text string) error {
	return &ErrorStartTime{
		text:     text,
		err_time: time.Now().String(),
	}
}

func make_file(name_file string) {
	f, err := os.Create(name_file)
	if err != nil {
		fmt.Printf("error on create %s\n", err)
	}
	defer f.Close()
}

func main() {
	var err error
	err = New("new_error")
	fmt.Println(err)

	make_file("test_file.txt")
	if err := os.Remove("test_file.txt"); err != nil {
		fmt.Printf("error on remove %s\n", err)
	}

	defer func() {
		if f_panic := recover(); f_panic != nil {
			fmt.Println("Don't panic!", f_panic)
		}
	}()
	pan.Pan()
}
