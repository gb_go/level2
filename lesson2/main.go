package main

import (
	"fmt"
	"os"
	"pan/pan"
	"time"
)

type errorStartTime struct {
	text    string
	errTime time.Time
}

func (e *errorStartTime) Error() string {
	return fmt.Sprintf("Error: %s\nat time: %s\n", e.text, e.errTime)
}

func new(text string) error {
	return &errorStartTime{
		text:    text,
		errTime: time.Now(),
	}
}

func makeFile(nameFile string) {
	f, err := os.Create(nameFile)
	defer f.Close()
	if err != nil {
		fmt.Printf("Error on create %s\n", err)
	}
	if _, err := f.WriteString("test"); err != nil {
		fmt.Printf("Error on write %s\n", err)
	}
}

func main() {
	var err error
	err = new("new_error")
	fmt.Println(err)

	makeFile("test_file.txt")
	if err := os.Remove("test_file.txt"); err != nil {
		fmt.Printf("Error on remove %s\n", err)
	}

	defer func() {
		if fPanic := recover(); fPanic != nil {
			fmt.Println("Don't panic!", fPanic)
		}
	}()
	pan.Pan()
}
