package main

import (
	"fmt"
	"os"
	"runtime/trace"
	"sync"
)

func useMutex() {
	trace.Start(os.Stderr)
	defer trace.Stop()

	var (
		c     int
		count = 1000
		wg    sync.WaitGroup
		mtx   sync.Mutex
	)
	wg.Add(count)
	for i := 0; i < count; i += 1 {
		go func(i int) {
			defer wg.Done()
			mtx.Lock()
			defer mtx.Unlock()
			c += i
		}(i)
	}
	wg.Wait()
	fmt.Println("With Mutex: ", c)
}

func main() {
	useMutex()
}
