package main

import (
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

func workers(max int) {
	var wg sync.WaitGroup
	var count int
	work := func(i int, c *int) {
		fmt.Println(i)
		*c++
	}

	for i := 1; i <= max; i++ {
		wg.Add(1)
		go func(n int) {
			defer wg.Done()
			work(n, &count)
		}(i)
	}

	wg.Wait()
	fmt.Println("Counts:", count)
}

func sigterm() {
	command := make(chan os.Signal, 1)
	signal.Notify(command, syscall.SIGINT, syscall.SIGTERM)

	ticker := time.NewTicker(time.Second)

	defer ticker.Stop()

	for {
		select {
		case sig := <-command:
			fmt.Printf("The end! Time end: %s. End by signal: %s\n", time.Now(), sig)
			return
		case res := <-ticker.C:
			fmt.Println(res)
		}
	}
}

func main() {
	workers(1000)
	sigterm()
}
