package main

import (
	"fmt"
	"sync"
)

func raceConditions() {
	var (
		c     int
		count = 1000
		wg    sync.WaitGroup
	)
	wg.Add(count)
	for i := 0; i < count; i += 1 {
		go func(i int) {
			defer wg.Done()
			c += i
		}(i)
	}
	wg.Wait()
	fmt.Println("Without Mutex: ", c)
}

func main() {
	raceConditions()
}
