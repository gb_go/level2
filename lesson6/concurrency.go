package main

import (
	"fmt"
	"runtime"
	"sync"
)

func useConcurrency() {
	runtime.GOMAXPROCS(1)

	var (
		wgEven sync.WaitGroup
		wgOdd  sync.WaitGroup
		count  = 1000
		c      int
	)

	wgEven.Add(count)
	wgOdd.Add(count)

	for i := 0; i < count; i++ {
		go func(i int) {
			defer wgEven.Done()
			if i%2 == 0 {
				fmt.Println("Even: ", i)
				c++
				runtime.Gosched()
			}
		}(i)
		go func(i int) {
			defer wgOdd.Done()
			if i%2 != 0 {
				fmt.Println("Odd: ", i)
				c++
				runtime.Gosched()
			}
		}(i)
	}
	wgEven.Wait()
	wgOdd.Wait()
	fmt.Println("Count: ", c)
}

func main() {
	useConcurrency()
}
