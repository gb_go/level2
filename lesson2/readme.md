# Build in Windows PowerShel
### for windows
`go build -o .\build\main.exe main.go`
### for linux
`go env -w GOOS=linux`

`go build -o .\build\main main.go`

# Очистит build директорию
`rm -r -Force .\build`
# Создать документацию
`godoc -http=:6060`

Документация будет доступна по http://localhost:6060/pkg/pan/pan/